import Vue from 'vue'
import Application from './components/App'

new Vue({
    el: '#app',
    name: 'ElikoTestTask',
    render: createElement => createElement(Application)
})