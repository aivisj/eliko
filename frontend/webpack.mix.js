//  lib load
let mix = require('laravel-mix')

mix.setPublicPath('./').js('src/frontend.js', './dist/app.js').sass('src/app.scss', './dist/app.css')
