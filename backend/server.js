'use strict'

const Twitter = require('twitter'),
    _ = require('lodash'),
    //  classes
    {Country} = require('../classes/Country'),
    {Trend} = require('../classes/Trend'),
    //  config for twitter and webserver
    _Config = require('./config')

/*
    TWITTER api keys / auth data
 */
const client = new Twitter({
    consumer_key: _Config.cKey,
    consumer_secret: _Config.cSecret,
    access_token_key: _Config.atKey,
    access_token_secret: _Config.atSecret
})

//  WOEIDs
let promises = [], countries = [
    new Country(23424829),   //  Germany
    new Country(23424975),   //  UK
    new Country(23424977),   //  USA
], argv = require('yargs-parser')(process.argv.slice(2)), refreshStartedAt = false, refreshInterval = argv.interval || 60000

/*
    get TRENDS from twitter (TRENDS are cached every 5 minutes)
    function is asynchronous
 */
const getCountryData = async (country) => {
    //
    return await new Promise((resolve, reject) => {
        //
        client.get('trends/place.json', {id: country.getId()}, (error, places, response) => {
            //
            if (!error) {
                //
                let [place] = places
                let [location] = place.locations
                //  used to work with getters & setters
                place.trends.forEach((trend, index) => {
                    place.trends[index] = new Trend(trend.name, trend.url, trend.promoted_content, trend.query, trend.tweet_volume)
                })
                //  ordering
                place.trends = _.orderBy(place.trends, ['tweetVolume'], ['desc']);
                //
                country.setName(location.name)
                country.Trends().setTrends(place.trends)
                // console.log(country.Trends().getTrends())
                resolve(country)
            } else {
                reject(error)
            }
        })
    })
}

/*
    put twitter api calls into promise array
 */
let twitterData = () => {
    //
    promises = []
    //
    _.forEach(countries, country => {
        promises.push(getCountryData(country))
    })
    //
    return promises
}


/*
    eXpress, websockets, mustache template
 */
const express = require('express'),
    path = require('path'),
    mustacheExpress = require('mustache-express'),
    app = express(),
    http = require('http').Server(app),
    io = require('socket.io')(http)

/*
    data refresh throught websocket
 */
let _refresh = () => {
    //  prevent dublicate run
    if(!refreshStartedAt){
        //  run this one
        refreshStartedAt = new Date()
        //  refresh UI data every 5sec
        setInterval(() => {

            Promise.all(twitterData()).then(results => {
                //  push data to UI
                io.emit('received countries data', results)
            }).catch(error => console.log(error))
        }, refreshInterval)
    }
}
//  activate UI refresh
_refresh()

/*
    data which need to be loaded immediately in UI
 */
let _data = Promise.all(twitterData()).then(results => {
    //  return full countries list with trends
    return results
}).catch(error => console.log(error))

/*
    express config
 */
app.engine('html', mustacheExpress())
app.set('view engine', 'html')
app.set('views', path.resolve(`${__dirname}/../frontend`))
app.use(express.static(`${__dirname}/../frontend/dist`))

/*
    express routes
 */
app.get('/', function (req, res) {
    _data.then(d => {
        //  render view
        res.render('index', {viewData: JSON.stringify(d)})
    })
})

/*
    starting web server
*/
http.listen(_Config.hostPort, () => {
    //  show frontend url for user
    console.log(`Eliko test task server running at http://localhost:${_Config.hostPort}`)
})