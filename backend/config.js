//
let params = {
    //  Consumer API keys
    cKey: '',  //  API key
    cSecret: '',  //  API secret key

    //  Access token & access token secret
    atKey: '',    //  Access token
    atSecret: '',      //  Access token secret

    //  Webserver adn websocket listening port
    hostPort: 35456 //  eliko
}
//
module.exports = params