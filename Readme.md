#Eliko test task

![Node.js](./images/nodejs.png) ![NPM](./images/npm.png)    ![ExpressJS](./images/expressjs.png)    

![Vue.JS](./images/vuejs.png)   ![Socket.IO](./images/socketio.png) ![Lodash](./images/lodash.png)  

![JavaScript](./images/js.png)  ![TypeScript](./images/ts.png)  ![ES6](./images/es6.png)    

![SASS](./images/sass.png)  ![Mustache](./images/mustache.png)  ![Twitter](./images/twitter.png)

#Configuration and installation details

Node.JS and NPM need to be installed on PC/Server.

##Back-end
Open CLI and navigate to backend folder:
```
1. cd backend/
```
Install dependencies for server:
```
2. npm install
```
Now need to change __Consumer API keys__ and __Access token & access token secret__ which are located into __/backend/config.js__ file:

```javascript
let params = {
    //  Consumer API keys
    cKey: '',  //  API key
    cSecret: '',  //  API secret key

    //  Access token & access token secret
    atKey: '',    //  Access token
    atSecret: '',      //  Access token secret

    //  Webserver adn websocket listening port
    hostPort: 35456 //  eliko
}
```
After success dependencies installation and config key filling can start server:
```
3. node server
```
or
```
3. node server.js
```
After success starting in console will be visible text like this:
```
Eliko test task server running at http://localhost:35456
```

##Front-end

If server is started than frontend is visible at address __http://localhost:35456__. 

###Notes
Server port can be changed into __/backend/config.js__ file. Webserver and websocket is using same port. On server starting can set refresh time in miliseconds by setting refresh interval with argument __--interval 3000__ (3000ms is refresh interval), default refresh interval in ms is __5000__.


Start server with user passed interval (3000ms or 3sec)
```
node server.js --interval=3000
```


