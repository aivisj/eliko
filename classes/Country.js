"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Trends_1 = require("./Trends");
var Country = /** @class */ (function () {
    function Country(woeid) {
        if (woeid === void 0) { woeid = 1; }
        this.woeid = woeid; //  set worldwide as default
        this.trends = new Trends_1.default();
    }
    Country.prototype.getId = function () {
        return this.woeid;
    };
    Country.prototype.setName = function (name) {
        this.name = name;
    };
    Country.prototype.Trends = function () {
        return this.trends;
    };
    return Country;
}());
exports.Country = Country;
