import {Trend} from './Trend'

type trends = Array<Trend>;

export default class Trends {
    list: trends;

    constructor(trendsList: trends = []) {
        this.list = trendsList
    }

    getTrends() {
        return this.list
    }

    addTrend(trend: Trend){
        this.list.push(trend)
    }

    setTrends(trendsList: trends) {
        this.list = trendsList
    }
}