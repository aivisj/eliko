import Trends from './Trends'

export class Country {
    woeid: number
    name: string
    trends: Trends

    constructor(woeid: number = 1){
        this.woeid = woeid  //  set worldwide as default
        this.trends = new Trends()
    }

    getId(){
        return this.woeid
    }

    setName(name){
        this.name = name
    }

    Trends(){
        return this.trends
    }
}