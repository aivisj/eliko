"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Trend = /** @class */ (function () {
    function Trend(name, url, promotedContent, query, tweetVolume) {
        this.setName(name || null);
        this.setUrl(url || null);
        this.setPromotedContent(promotedContent || null);
        this.setQuery(query || null);
        this.setVolume(tweetVolume || 0);
    }
    Trend.prototype.getName = function () {
        return this.name;
    };
    Trend.prototype.setName = function (name) {
        this.name = name;
    };
    Trend.prototype.getUrl = function () {
        return this.url;
    };
    Trend.prototype.setUrl = function (url) {
        this.url = url;
    };
    Trend.prototype.getVolume = function () {
        return this.tweetVolume;
    };
    Trend.prototype.setVolume = function (volume) {
        this.tweetVolume = volume;
    };
    Trend.prototype.getQuery = function () {
        return this.query;
    };
    Trend.prototype.setQuery = function (q) {
        this.query = q;
    };
    Trend.prototype.getPromotedContent = function () {
        return this.promotedContent;
    };
    Trend.prototype.setPromotedContent = function (promoCont) {
        this.promotedContent = promoCont;
    };
    return Trend;
}());
exports.Trend = Trend;
