export class Trend {
    name: string
    url: string
    promotedContent: string
    query: string
    tweetVolume: number

    constructor(name: string, url: string, promotedContent: string, query: string, tweetVolume: number) {
        this.setName(name || null)
        this.setUrl(url || null)
        this.setPromotedContent(promotedContent || null)
        this.setQuery(query || null)
        this.setVolume(tweetVolume || 0)
    }

    getName(){
        return this.name
    }

    setName(name){
        this.name = name
    }

    getUrl(){
        return this.url
    }

    setUrl(url){
        this.url = url
    }

    getVolume(){
        return this.tweetVolume
    }

    setVolume(volume){
        this.tweetVolume = volume
    }

    getQuery(){
        return this.query
    }

    setQuery(q){
        this.query = q
    }

    getPromotedContent(){
        return this.promotedContent
    }

    setPromotedContent(promoCont){
        this.promotedContent = promoCont
    }
}