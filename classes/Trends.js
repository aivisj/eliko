"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Trends = /** @class */ (function () {
    function Trends(trendsList) {
        if (trendsList === void 0) { trendsList = []; }
        this.list = trendsList;
    }
    Trends.prototype.getTrends = function () {
        return this.list;
    };
    Trends.prototype.addTrend = function (trend) {
        this.list.push(trend);
    };
    Trends.prototype.setTrends = function (trendsList) {
        this.list = trendsList;
    };
    return Trends;
}());
exports.default = Trends;
